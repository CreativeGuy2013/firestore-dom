"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var fsdom = function () {
    function fsdom() {
        _classCallCheck(this, fsdom);
    }

    _createClass(fsdom, [{
        key: "initializeApp",
        value: function initializeApp(config) {
            var app = firebase.initializeApp(config);
            this.firestore = app.firestore();

            this.getCollections();
        }
    }, {
        key: "getCollections",
        value: function getCollections() {
            var _this = this;

            var collections = document.querySelectorAll("*[data-fs-collection]");

            collections.forEach(function (collection) {
                var collectionPath = collection.getAttribute("data-fs-collection");
                var template = collection.querySelector("*[data-fs-collection=\"" + collectionPath + "\"]>*[data-fs-template]");
                _this.firestore.collection(collectionPath).get().then(function (data) {
                    console.log(data);
                    data.docs.forEach(function (doc) {
                        var newNode = document.createElement(template.tagName);
                        var docData = doc.data();
                        newNode.innerHTML = template.innerHTML;
                        newNode.setAttribute("id", collectionPath + "_" + doc.id);
                        newNode.querySelectorAll("#" + collectionPath + "_" + doc.id + " *[data-fs-data]").forEach(function (item) {
                            console.log(item);
                            item.innerHTML = docData[item.getAttribute("data-fs-data")];
                        });
                        collection.appendChild(newNode);
                    });
                    collection.querySelector("*[data-fs-collection=\"" + collectionPath + "\"]>*[data-fs-loading]").setAttribute("hidden", "hidden");
                }).catch(function (error) {
                    console.error(error);
                });
            });
        }
    }]);

    return fsdom;
}();