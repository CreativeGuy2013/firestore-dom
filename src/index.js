

class fsdom {
    initializeApp(config) {
        var app = firebase.initializeApp(config)
        this.firestore = app.firestore()    
    
        this.getCollections()
    }

    getCollections() {
        var collections = document.querySelectorAll("*[data-fs-collection]")

        collections.forEach(collection => {
            var collectionPath = collection.getAttribute("data-fs-collection")
            var template = collection.querySelector("*[data-fs-collection=\"" + collectionPath + "\"]>*[data-fs-template]")
            this.firestore.collection(collectionPath).get().then((data) => {
                console.log(data)
                data.docs.forEach(doc => {
                    var newNode = document.createElement(template.tagName)
                    var docData = doc.data()
                    newNode.innerHTML = template.innerHTML
                    newNode.setAttribute("id", collectionPath + "_" + doc.id)
                    newNode.querySelectorAll("#" + collectionPath + "_" + doc.id + " *[data-fs-data]").forEach(item => {
                        console.log(item)
                        item.innerHTML = docData[item.getAttribute("data-fs-data")]
                    })
                    collection.appendChild(newNode)
                })
                collection.querySelector("*[data-fs-collection=\"" + collectionPath + "\"]>*[data-fs-loading]").setAttribute("hidden", "hidden")
            }).catch((error) => {
                console.error(error)
            })
        });
    }
}
